#!/usr/bin/env bash

# Check if command exists
if [[ $# -lt 1 ]]; then
    echo -e "A command is required (up|down|config)!"
    exit
fi

docker_dir=$(pwd)/docker
env_file=${docker_dir}/.env
docker_compose_file=${docker_dir}/docker-compose.yml

# Getting environment vars from .env
envs_string=$(grep -v '^#' ${env_file} | xargs)
read -r -a envs_array <<< $envs_string

# Export environment vars
for element in ${envs_array[@]}
do
    export $element
done


# Execute command
if [[ "$1" == 'up' ]]; then
    docker-compose -f $docker_compose_file up --build -d

    APP_HOST=$(docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${PROJECT_NAME}_app)
    POSTGRES_HOST=$(docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${PROJECT_NAME}_postgres)

    # Print links to services
    echo
    echo "List of services IPs:"
    echo "App: http://${APP_HOST}:${RECIPE_MANAGER_APP__PORT}"
    echo "Postgres: {'host': ${POSTGRES_HOST}, 'port': ${RECIPE_MANAGER_POSTGRES__PORT}}"
elif [[ "$1" == "down" ]]; then
    docker-compose -f $docker_compose_file down
elif [[ "$1" == "config" ]]; then
    docker-compose -f $docker_compose_file config
else
    echo -e "Provided command is invalid. Exiting..."
    exit
fi
