"""Flask resources."""
from typing import Tuple

from flask import request
from flask_restful import Resource
from http import HTTPStatus

from models.recipe import Recipe, recipe_list


class BaseResource(Resource):
    @property
    def _not_found(self) -> Tuple[dict, int]:
        return {'message': 'recipe not found'}, HTTPStatus.NOT_FOUND


class RecipeListResource(BaseResource):
    """Recipes resource."""
    def get(self):
        data = tuple(recipe.data for recipe in recipe_list if recipe.is_publish)
        return {'data': data}, HTTPStatus.OK

    def post(self):
        data = request.get_json()
        recipe = Recipe(**data)
        recipe_list.append(recipe)
        return recipe.data, HTTPStatus.CREATED


class RecipeResource(BaseResource):
    """Single recipe resource."""
    def get(self, recipe_id: int):
        recipe = next((recipe for recipe in recipe_list if recipe.id == recipe_id and recipe.is_publish is True), None)
        if recipe is None:
            return self._not_found

        return recipe.data, HTTPStatus.OK

    def put(self, recipe_id: int):
        data = request.get_json()
        recipe = next((recipe for recipe in recipe_list if recipe_id == recipe_id), None)
        if recipe is None:
            return self._not_found

        recipe.name = data.get('name', recipe.name)
        recipe.description = data.get('description', recipe.description)
        recipe.num_of_servings = data.get('num_of_servings', recipe.num_of_servings)
        recipe.cook_time = data.get('cook_time', recipe.cook_time)
        recipe.directions = data.get('directions', recipe.directions)

        return recipe.data, HTTPStatus.OK

    def delete(self, recipe_id):
        recipe = next((recipe for recipe in recipe_list if recipe_id == recipe_id), None)
        if recipe is None:
            return self._not_found

        recipe_list.remove(recipe)
        return {}, HTTPStatus.NO_CONTENT


class RecipePublishResource(BaseResource):
    """Publish resource."""
    def put(self, recipe_id: int):
        recipe = next((recipe for recipe in recipe_list if recipe.id == recipe_id), None)
        if recipe is None:
            return self._not_found

        recipe.is_publish = True
        return {}, HTTPStatus.NO_CONTENT

    def delete(self, recipe_id: int):
        recipe = next((recipe for recipe in recipe_list if recipe.id == recipe_id), None)
        if recipe is None:
            return self._not_found

        recipe.is_publish = False
        return {}, HTTPStatus.NO_CONTENT
