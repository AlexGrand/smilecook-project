from flask import Flask, current_app
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, init, migrate as m, upgrade

from common.config import config, SQLAlchemyConfig

db = SQLAlchemy()
migrate = Migrate()


def create_app(name: str = None):
    app = Flask(name or config['app']['name'].get())
    app.config.from_object(SQLAlchemyConfig)

    app.app_context().push()

    db.init_app(app)
    migrate.init_app(app, db)

    @current_app.route('/')
    def home():
        return 'Hello, World!'

    print(">>> app.config", current_app.config)

    return app


class User(db.Model):
    # __table_args__ = {'schema': 'recipes'}
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
