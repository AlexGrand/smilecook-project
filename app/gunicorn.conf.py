from pathlib import Path

from common.config import config as conf

gunicorn_cfg = conf['gunicorn']
app_cfg = conf['app']

bind = f"{app_cfg['host']}:{app_cfg['port']}"
workers = gunicorn_cfg['workers'].get(int)
worker_class = gunicorn_cfg['worker_class'].get()
wsgi_app = gunicorn_cfg['wsgi_app'].get()
chdir = gunicorn_cfg['chdir'].get() or str(Path(__file__).parent.absolute())
reload = gunicorn_cfg['reload'].get()
loglevel = gunicorn_cfg['loglevel'].get()
