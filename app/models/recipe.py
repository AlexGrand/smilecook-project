"""Recipe model."""
from dataclasses import dataclass, asdict


recipe_list = []


def get_last_id():
    if recipe_list:
        last_recipe = recipe_list[-1]
    else:
        return 1
    return last_recipe.id + 1


class Recipe:
    def __init__(self, name: str, description: str, num_of_servings: int, cook_time: int, directions: str):
        self.name = name
        self.description = description
        self.num_of_servings = num_of_servings
        self.cook_time = cook_time
        self.directions = directions
        self.is_publish = False
        self.id = get_last_id()

    @property
    def data(self):
        return {key: val for key, val in self.__dict__.items() if not key == 'is_publish'}
