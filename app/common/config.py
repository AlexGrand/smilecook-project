from dataclasses import dataclass
from pathlib import Path
import os

from common.cfg.base_config_parser import BaseConfigParser


CONFIG_PATH = Path(__file__).parent.parent.joinpath('config.yml').as_posix()

config = BaseConfigParser(appname=os.environ.get('PROJECT_NAME'), path=CONFIG_PATH)


@dataclass
class Postgres:
    host: str = config['postgres']['host'].get()
    port: int = config['postgres']['port'].get(int)
    user: str = config['postgres']['user'].get()
    password: str = config['postgres']['password'].get()
    db: str = config['postgres']['db'].get()


@dataclass
class SQLAlchemyConfig:
    SQLALCHEMY_DATABASE_URI: str = config['sqlalchemy']['sqlalchemy_database_uri'].get() or\
                                   f"postgresql://{Postgres.user}:{Postgres.password}" \
                                   f"@{Postgres.host}:{Postgres.port}/{Postgres.db}"
    SQLALCHEMY_TRACK_MODIFICATIONS: str = config['sqlalchemy']['track_modifications'].get(bool)
