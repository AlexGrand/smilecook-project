from dataclasses import dataclass


@dataclass
class ApiPath:
    recipes: str = '/recipes'
    recipe: str = '/recipes/<int:recipe_id>'
    recipe_publish: str = '/recipes/<int:recipe_id>/publish'
