from typing import Union
import os
from pathlib import Path

import confuse
import yaml


class YamlLoader(yaml.SafeLoader):
    """Basic Yaml loader. Uses confuse.Loader constructors."""
    def __init__(self, stream):
        super(YamlLoader, self).__init__(stream)
        confuse.Loader.add_constructors(self)


class BaseConfigParser(confuse.Configuration):
    def __init__(
            self, appname: str, path: Union[str, Path] = None, modname: str = __name__, read: bool = True,
            loader: yaml.Loader = YamlLoader):
        super(BaseConfigParser, self).__init__(
            appname=appname,
            modname=modname,
            read=read,
            loader=loader
        )
        self._path = path

        if self._path:
            self.set_file(self._path)

        self.set_env()
