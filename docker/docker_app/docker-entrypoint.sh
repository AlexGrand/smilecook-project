#!/usr/bin/env bash

set -e

: "${OPTIONS:=}"

# Check if command exists
if [[ $# -lt 1 ]]; then
    echo -e "A command is required!"
    exit
fi

# Execute command
if [ "$1" = 'start' ]; then
  echo -e "\nStarting service in development mode ..."
exec gunicorn -c gunicorn.conf.py ${OPTIONS}
else
    exec "$@"
fi

